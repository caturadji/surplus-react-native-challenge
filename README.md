# Surplus React Native Challenge

## Screen preview

|                                                     Login                                                     |                                                    Register                                                    |                                                Forgot Password                                                 |                                                      Home                                                      |
| :-----------------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------: |
| <img src="https://i.ibb.co/cX2jbmQ/Simulator-Screen-Shot-i-Phone-14-2023-06-09-at-21-34-24.png" width="200" > | <img src="https://i.ibb.co/42cvQQp/Simulator-Screen-Shot-i-Phone-14-2023-06-09-at-21-34-33.png" width="200" /> | <img src="https://i.ibb.co/YDMqd7V/Simulator-Screen-Shot-i-Phone-14-2023-06-09-at-21-34-43.png" width="200" /> | <img src="https://i.ibb.co/thyMHtm/Simulator-Screen-Shot-i-Phone-14-2023-06-09-at-21-43-34.png" width="200" /> |

## How to run

1. Demo (Simulator: Iphone 14): [Demo Link](https://drive.google.com/file/d/1OHOKbdhk5sZutGIBD-TmnJBJvarQoTQW/view?usp=sharing)
2. Demo (Real device: Samsung A53): [Demo Link](https://drive.google.com/file/d/12xJxgEjl2nEAlqNdobKTLNmTS5FnTQb-/view?usp=sharing)

### How to build

1. Setup React Native environment in your machine : [React Native Environment Setup Documentation](https://reactnative.dev/docs/environment-setup)
2. Clone this repository to your local directory
3. Open terminal with your clone repository as a root
4. Install depedencies

```
 npm install
```

5. a. Build on your android emulator

```
 npm run android
```

5. B. 1. Install framework added for ios development

```
 cd ios && pod install && cd ..
```

5. B. 2. Build on your ios simulator

```
 npm run ios
```

### How to install .apk file

Download .apk file here: [Download Link](https://drive.google.com/file/d/1WE1A3WRpyH6LE71z6G4bwvQzevag4pnS/view?usp=sharing)

### How to run with .ipa file

Download .ipa file here: [Download Link]()

#### Using simulator

1. Rename .ipa file as .zip and extract the contents
2. Once the zip file is extracted, we can find the Payload folder which contains App_Name.app file
3. Drag & drop App_Name.app file to ios simulator

#### Using ios device

1. Upload the .ipa to any such site which create a shareable installation link using the uploaded .ipa file. Our recomendation is https://www.installonair.com
2. Open the above link and upload the downloaded .ipa file
3. Once the files get uploaded click on submit button and wait for the shareable link to get generated
4. Lastly install the app on the mobile device by opening the shared link through installonair and you are are ready to test once the app gets installed on your device.
