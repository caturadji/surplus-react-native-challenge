export const lightColors = {
  appPrimary: '#fbfcfd',
  appSecondary: '#1f54d3',
  labelPrimary: '#1e2944',
  labelSecondary: '#707070',
  placeholder: '#C0C0C0',
  separator: '#C0C0C0',
};
