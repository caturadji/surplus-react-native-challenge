import React, {useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {lightColors} from '../../../src/assets/styles/colors';
import Input from '../../../src/components/input/input';
import {useNavigation} from '@react-navigation/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

function Register(): JSX.Element {
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const navigation = useNavigation();

  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      contentContainerStyle={{height: '100%'}}>
      <View style={styles.container}>
        <MaterialIcons
          onPress={() => navigation.goBack()}
          name="chevron-left"
          size={30}
          color={lightColors.labelPrimary}
          style={styles.back}
        />
        <Image
          resizeMode="cover"
          source={require('../../assets/images/SignUp.jpg')}
          style={styles.image}
        />
        <Text style={styles.title}>Register</Text>
        <View style={styles.section}>
          <Input
            placeholder="Email"
            icon={
              <MaterialIcons
                name="alternate-email"
                size={25}
                color={lightColors.placeholder}
              />
            }
            value={email}
            onChangeText={(value: string) => setEmail(value)}
          />
          <Input
            placeholder="Name"
            icon={
              <MaterialIcons
                name="person-outline"
                size={25}
                color={lightColors.placeholder}
              />
            }
            value={name}
            onChangeText={(value: string) => setName(value)}
          />
          <Input
            placeholder="Phone"
            icon={
              <SimpleLineIcons
                name="phone"
                size={20}
                color={lightColors.placeholder}
              />
            }
            value={phone}
            onChangeText={(value: string) => setPhone(value)}
          />
        </View>
        <Text style={styles.desc}>
          By signing up, you're agree to our{' '}
          <Text
            style={{
              color: lightColors.appSecondary,
            }}>
            Terms & Conditions
          </Text>{' '}
          and
          <Text
            style={{
              color: lightColors.appSecondary,
            }}>
            {' '}
            Privacy Policy
          </Text>
        </Text>
        <TouchableOpacity onPress={() => {}} style={styles.button}>
          <Text style={styles.buttonText}>Register</Text>
        </TouchableOpacity>
        <View style={styles.footer}>
          <Text style={styles.footerText}>Joined us before?</Text>
          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <Text style={styles.linkText}>Login</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: lightColors.appPrimary,
    padding: 20,
  },
  back: {
    position: 'absolute',
    top: 20,
    left: 10,
    zIndex: 5,
  },
  image: {
    width: '100%',
    height: '40%',
  },
  title: {
    color: lightColors.labelPrimary,
    fontSize: 40,
    fontWeight: 'bold',
  },
  section: {
    marginTop: 5,
  },
  desc: {
    marginTop: 10,
    color: lightColors.labelPrimary,
  },
  button: {
    padding: 15,
    backgroundColor: lightColors.appSecondary,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '700',
    color: lightColors.appPrimary,
  },
  footer: {
    width: '100%',
    marginTop: 25,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footerText: {
    fontSize: 14,
    fontWeight: '500',
    color: lightColors.labelPrimary,
    marginRight: 3,
  },
  linkText: {
    fontSize: 14,
    fontWeight: '500',
    color: lightColors.appSecondary,
  },
});

export default Register;
