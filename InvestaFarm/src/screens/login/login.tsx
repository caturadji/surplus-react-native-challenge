import React, {useEffect, useState} from 'react';
import {
  Alert,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {lightColors} from '../../../src/assets/styles/colors';
import Input from '../../../src/components/input/input';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

function Login(): JSX.Element {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [showPassword, setShowPassword] = useState<boolean>(false);

  const navigation = useNavigation();

  useEffect(() => {
    getToken();
    setTimeout(() => {
      SplashScreen.hide();
    }, 500);
  }, []);

  const getToken = async () => {
    const token = await AsyncStorage.getItem('token');
    if (token && token !== '') {
      navigation.reset({
        index: 0,
        routes: [{name: 'Main'}],
      });
    }
  };

  const onLogin = async () => {
    const emailValid = 'admin@mail.com';
    const passwordValid = '1234';
    if (
      password.toLowerCase().trim() === passwordValid &&
      email.toLowerCase().trim() === emailValid
    ) {
      await AsyncStorage.setItem('token', email);
      navigation.reset({
        index: 0,
        routes: [{name: 'Main'}],
      });
    } else {
      Alert.alert('Error', 'Email or password incorrect');
    }
  };

  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      contentContainerStyle={{height: '100%'}}>
      <View style={styles.container}>
        <Image
          resizeMode="cover"
          source={require('../../assets/images/Login.jpg')}
          style={styles.image}
        />
        <View style={styles.section}>
          <Text style={styles.title}>Login</Text>
          <View style={styles.section}>
            <Input
              placeholder="Email"
              icon={
                <MaterialIcons
                  name="alternate-email"
                  size={25}
                  color={lightColors.placeholder}
                />
              }
              value={email}
              onChangeText={(value: string) => setEmail(value)}
              inputMode="email"
            />
            <Input
              placeholder="Password"
              icon={
                <MaterialIcons
                  name="lock-outline"
                  size={25}
                  color={lightColors.placeholder}
                />
              }
              value={password}
              onChangeText={(value: string) => setPassword(value)}
              secureTextEntry={true}
            />
          </View>
          <View style={styles.forgot}>
            <TouchableOpacity
              style={styles.clickable}
              onPress={() => navigation.navigate('Forgot')}>
              <Text style={styles.body}>Forgot Password?</Text>
            </TouchableOpacity>
          </View>
        </View>

        <TouchableOpacity onPress={onLogin} style={styles.button}>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 10,
          }}>
          <View
            style={{
              height: 0.5,
              width: '40%',
              backgroundColor: lightColors.separator,
            }}
          />
          <Text
            style={{
              color: lightColors.separator,
              width: '20%',
              textAlign: 'center',
            }}>
            OR
          </Text>
          <View
            style={{
              height: 0.5,
              width: '40%',
              backgroundColor: lightColors.separator,
            }}
          />
        </View>

        <TouchableOpacity
          onPress={() => {}}
          style={[styles.button, styles.buttonRow]}>
          <Image
            resizeMode="cover"
            source={require('../../assets/images/Google.png')}
            style={styles.logo}
          />
          <Text
            style={[
              styles.buttonText,
              {
                color: lightColors.labelPrimary,
              },
            ]}>
            Login with Google
          </Text>
        </TouchableOpacity>

        <View style={styles.footer}>
          <Text style={styles.footerText}>New to Investa?</Text>
          <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text style={styles.linkText}>Register</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: lightColors.appPrimary,
    padding: 20,
  },
  image: {
    width: '100%',
    height: '40%',
  },
  title: {
    color: lightColors.labelPrimary,
    fontSize: 40,
    fontWeight: 'bold',
  },
  section: {
    marginTop: 10,
  },
  forgot: {
    alignItems: 'flex-end',
    marginVertical: 10,
  },
  clickable: {
    width: '40%',
  },
  body: {
    fontSize: 14,
    fontWeight: '500',
    color: lightColors.appSecondary,
    textAlign: 'right',
  },
  button: {
    padding: 15,
    backgroundColor: lightColors.appSecondary,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  buttonRow: {
    backgroundColor: '#e9edee',
    flexDirection: 'row',
    marginVertical: 0,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '700',
    color: lightColors.appPrimary,
  },
  footer: {
    width: '100%',
    marginTop: 25,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footerText: {
    fontSize: 14,
    fontWeight: '500',
    color: lightColors.labelPrimary,
    marginRight: 3,
  },
  linkText: {
    fontSize: 14,
    fontWeight: '500',
    color: lightColors.appSecondary,
  },
  logo: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
});

export default Login;
