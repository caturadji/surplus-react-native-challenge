import React, {useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {lightColors} from '../../../src/assets/styles/colors';
import Input from '../../../src/components/input/input';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useNavigation} from '@react-navigation/native';

function Forgot(): JSX.Element {
  const [email, setEmail] = useState('');
  const navigation = useNavigation();
  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      contentContainerStyle={{height: '100%'}}>
      <View style={styles.container}>
        <MaterialIcons
          onPress={() => navigation.goBack()}
          name="chevron-left"
          size={30}
          color={lightColors.labelPrimary}
          style={styles.back}
        />
        <Image
          resizeMode="cover"
          source={require('../../assets/images/ForgotPassword.jpg')}
          style={styles.image}
        />
        <Text style={styles.title}>Forgot Password?</Text>
        <Text style={styles.desc}>
          Don't worry! it happens. Please enter the address associated with your
          account
        </Text>
        <Input
          placeholder="Email"
          icon={
            <MaterialIcons
              name="alternate-email"
              size={25}
              color={lightColors.placeholder}
            />
          }
          value={email}
          onChangeText={(value: string) => setEmail(value)}
        />
        <TouchableOpacity onPress={() => {}} style={styles.button}>
          <Text style={styles.buttonText}>Submit</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: lightColors.appPrimary,
    padding: 20,
  },
  back: {
    position: 'absolute',
    top: 20,
    left: 10,
    zIndex: 5,
  },
  image: {
    width: '100%',
    height: '40%',
  },
  title: {
    color: lightColors.labelPrimary,
    fontSize: 40,
    fontWeight: 'bold',
  },
  section: {
    marginTop: 5,
  },
  desc: {
    color: lightColors.labelPrimary,
    marginTop: 15,
    marginBottom: 15,
  },
  button: {
    padding: 15,
    backgroundColor: lightColors.appSecondary,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '700',
    color: lightColors.appPrimary,
  },
  footer: {
    width: '100%',
    position: 'absolute',
    bottom: 20,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footerText: {
    fontSize: 14,
    fontWeight: '500',
    color: lightColors.labelPrimary,
    marginRight: 3,
  },
  linkText: {
    fontSize: 14,
    fontWeight: '500',
    color: lightColors.appSecondary,
  },
});

export default Forgot;
