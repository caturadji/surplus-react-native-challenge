import React, {useEffect, useState} from 'react';
import {
  Alert,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {lightColors} from '../../../src/assets/styles/colors';
import axios from 'axios';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNavigation} from '@react-navigation/native';
import {ScrollView} from 'react-native';

interface Character {
  created: string;
  episode: string[];
  gender: string;
  id: number;
  image: string;
  location: {
    name: string;
    url: string;
  };
  name: string;
  origin: {
    name: string;
    url: string;
  };
  species: string;
  status: string;
  type: string;
  url: string;
}

interface CharacterInfo {
  count: number;
  next: string | null;
  pages: number;
  prev: string | null;
}

interface CharacterData {
  info: CharacterInfo;
  results: Character[];
}

function Main(): JSX.Element {
  const [data, setData] = useState<CharacterData>();
  const navigation = useNavigation();

  useEffect(() => {
    axios
      .get('https://rickandmortyapi.com/api/character/?page=1')
      .then(res => {
        setData(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.title}>Home</Text>
        <MaterialIcons
          onPress={() => {
            Alert.alert('Logout', 'Are you sure you want to logout', [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {
                text: 'OK',
                onPress: async () => {
                  await AsyncStorage.removeItem('token');
                  navigation.reset({
                    index: 0,
                    routes: [{name: 'Login'}],
                  });
                },
              },
            ]);
          }}
          name="logout"
          size={30}
          color={lightColors.labelPrimary}
        />
      </View>
      <FlatList
        style={{paddingHorizontal: 20}}
        data={data?.results}
        keyExtractor={(item, index) => String(item.id + index)}
        renderItem={({item}) => {
          const colorStatus =
            item.status === 'Alive'
              ? 'green'
              : item.status === 'Dead'
              ? 'red'
              : lightColors.placeholder;
          return (
            <View style={styles.card}>
              <Image
                resizeMode="cover"
                source={{uri: item.image}}
                style={styles.image}
              />
              <View style={{padding: 10}}>
                <Text style={styles.headline}>{item.name}</Text>
                <View style={styles.row}>
                  <View
                    style={[
                      styles.status,
                      {
                        backgroundColor: colorStatus,
                      },
                    ]}
                  />
                  <Text style={styles.body}>{item.status} - </Text>
                  <Text
                    style={{
                      color: lightColors.labelPrimary,
                    }}>
                    {item.species}
                  </Text>
                </View>
                <View style={{marginTop: 5}}>
                  <Text style={{color: lightColors.labelSecondary}}>
                    Last location:
                  </Text>
                  <Text
                    style={{
                      color: lightColors.labelPrimary,
                    }}>
                    {item.location.name}
                  </Text>
                </View>
              </View>
            </View>
          );
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: lightColors.appPrimary,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
  },
  title: {
    color: lightColors.labelPrimary,
    fontSize: 40,
    fontWeight: 'bold',
  },
  card: {
    backgroundColor: '#F5F5F5',
    marginBottom: 10,
    borderRadius: 15,
    flexDirection: 'row',
    overflow: 'hidden',
  },
  image: {width: 100, height: '100%'},
  headline: {
    fontSize: 18,
    fontWeight: '700',
    color: lightColors.labelPrimary,
  },
  row: {flexDirection: 'row', alignItems: 'center'},
  status: {
    height: 6,
    width: 6,
    borderRadius: 100,
    marginRight: 5,
  },
  body: {
    color: lightColors.labelPrimary,
    fontWeight: '500',
  },
});

export default Main;
