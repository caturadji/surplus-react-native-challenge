import React, {ReactElement} from 'react';
import {StyleSheet, TextInput, View, TextInputProps} from 'react-native';
import {lightColors} from '../../assets/styles/colors';

interface InputProps extends TextInputProps {
  icon: ReactElement;
}

const Input = ({icon = <></>, ...rest}: InputProps) => {
  return (
    <View style={styles.inputContainer}>
      <View style={styles.iconContainer}>{icon}</View>
      <TextInput
        {...rest}
        placeholderTextColor={lightColors.separator}
        style={styles.textInput}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginVertical: 10,
  },
  textInput: {
    borderBottomWidth: 0.5,
    borderBottomColor: lightColors.separator,
    color: lightColors.labelPrimary,
    fontSize: 16,
    width: '90%',
    paddingVertical: 5,
  },
  iconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: 10,
    width: '10%',
  },
  icon: {
    height: 25,
    width: 25,
    tintColor: '#b2b2b2',
  },
});

export default Input;
